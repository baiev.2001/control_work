import control.Control;
import model.Post;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static model.ActivePost.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ControllerTest {
    private List<User> users;
    private Control control;

    @BeforeEach
    public void init() {
        control = new Control();
        List<Post> postsVictor = new ArrayList<>();
        postsVictor.add(new Post("wow1"));
        postsVictor.add(new Post("wow2"));
        postsVictor.add(new Post("wow3"));
        postsVictor.add(new Post("wow4"));
        postsVictor.add(new Post("wow5"));
        postsVictor.add(new Post("wow6"));
        postsVictor.add(new Post("wow7"));
        this.users = new ArrayList<>();
        User victor = new User("Victor", postsVictor);
        users.add(victor);
        List<Post> postsYuliya = new ArrayList<>();
        postsYuliya.add(new Post("yuliyaPost"));
        postsYuliya.add(new Post("yuliyaPost2"));
        postsYuliya.add(new Post("yuliyaPost3"));
        postsYuliya.add(new Post("yuliyaPost4"));
        postsYuliya.add(new Post("yuliyaPost5"));
        User yuliya = new User("Yuliya", postsYuliya);
        users.add(yuliya);
        List<Post> postsMichael = new ArrayList<>();
        postsMichael.add(new Post("michaelPost"));
        postsMichael.add(new Post("michaelPost2"));
        postsMichael.add(new Post("michaelPost3"));
        User michael = new User("Michael", postsMichael);
        users.add(michael);
    }

    @Test
    public void getUserMostPostsShouldReturnVictor() {
        assertEquals(control.getUserMostActivePosts(users).getName(), "Victor");
    }

    @Test
    public void getUserMostActivePostsShouldReturnYuliya() {
        users.get(0).getPosts().forEach(post -> post.setActive(NOT_ACTIVE));
        assertEquals(control.getUserMostActivePosts(users).getName(), "Yuliya");
    }

    @Test
    public void getUserByActivePostsAccess() {
        List<Post> postsDima = new ArrayList<>();
        postsDima.add(Post.builder()
                .active(NOT_ACTIVE)
                .content("dimaPost1")
                .build());
        postsDima.add(Post.builder()
                .active(NOT_ACTIVE)
                .content("dimaPost2")
                .build());
        postsDima.add(Post.builder()
                .active(NOT_ACTIVE)
                .content("dimaPost3")
                .build());
        postsDima.add(Post.builder()
                .active(ACTIVE)
                .content("dimaPost4")
                .build());
        User dima = new User("Dima", postsDima);
        users.add(dima);
        //Dima have only one Active post "dimaPost4"
        assertEquals(control.getUsersByActivePosts(users, 2), new ArrayList<>(Arrays.asList(dima)));
    }

    @Test
    public void getPostsByQuote() {

    }

    @Test
    public void getInactiveUsersAccess() {
        List<Post> postsDima = new ArrayList<>();
        postsDima.add(Post.builder()
                .time(LocalDate.of(2000, 1, 19))
                .content("dimaPost")
                .build());
        postsDima.add(Post.builder()
                .time(LocalDate.of(2000, 1, 19))
                .content("dimaPost2")
                .build());
        postsDima.add(Post.builder()
                .time(LocalDate.of(2000, 1, 19))
                .content("dimaPost3")
                .build());
        postsDima.add(Post.builder()
                .time(LocalDate.of(2000, 1, 19))
                .content("dimaPost4")
                .build());
        User dima = new User("Dima", postsDima);
        users.add(dima);
        assertEquals(control.getInactiveUsers(users), new ArrayList<>(Arrays.asList(dima)));
    }
}
