package control;

import model.ActivePost;
import model.Post;
import model.User;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Control {
    public User getUserMostPosts(List<User> users) {
        return users.stream().max(Comparator.comparing(user -> user.getPosts().size())).get();
    }

    public User getUserMostActivePosts(List<User> users) {
        return users.stream().max(Comparator.comparing(user -> user.getPosts().stream()
                .filter(post -> post.getActive() == ActivePost.ACTIVE).count())).get();
    }

    public List<User> getUsersByActivePosts(List<User> users, int num) {
        return users.stream()
                .filter(user -> user.getPosts().stream()
                        .filter(post -> post.getActive() == ActivePost.ACTIVE).count() < num)
                .collect(Collectors.toList());
    }

    public List<Post> getPostsByQuote(List<User> users, int num) {
        return users.stream()
                .flatMap(user -> user.getPosts().stream())
                .filter(post -> post.getNumOfQuotes() > num)
                .collect(Collectors.toList());
    }

    public List<User> getInactiveUsers(List<User> users) {
        //User is inactive if his las post was added more then one month ago
        return users.stream()
                .filter(user -> user.getPosts().stream()
                        .anyMatch(post -> ChronoUnit.MONTHS.between(post.getTime(), LocalDate.now()) > 1))
                .collect(Collectors.toList());
    }
}
