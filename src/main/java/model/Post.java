package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import postDecorator.AbstractPost;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post extends AbstractPost {
    private ActivePost active;
    private String content;
    private LocalDate time;
    private int numOfQuotes;
    private Post quote;

    {
        this.active = ActivePost.ACTIVE;
        this.time = LocalDate.now();
    }

    public Post(String content) {
        this.content = content;
    }

    public Post(String content, Post quote) {
        this.content = content;
        this.quote = quote;
    }

    public void setQuote(Post quote) {
        quote.numOfQuotes++;
        this.quote = quote;
    }
}
