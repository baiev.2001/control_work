package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class User {
    private String name;
    private List<Post> posts;

    public User(String name) {
        this.name = name;
    }
}
