package postDecorator;

import lombok.Data;

@Data
public class QuotedPost extends PostDecorator {
    private int numOfQuotes;
    private QuotedPost quote;

    public QuotedPost(AbstractPost post) {
        super(post);
    }

    public void setQuote(QuotedPost quote) {
        quote.numOfQuotes++;
        this.quote = quote;
    }
}
