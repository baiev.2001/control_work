package postDecorator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PostDecorator extends AbstractPost {
    private AbstractPost post;

    public PostDecorator(AbstractPost post) {
        this.post = post;
    }
}
