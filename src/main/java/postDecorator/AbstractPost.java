package postDecorator;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@SuperBuilder
@NoArgsConstructor
public abstract class AbstractPost {
    private String content;
    private LocalDate time;

    public AbstractPost(String content) {
        this.content = content;
        this.time = LocalDate.now();
    }
}
