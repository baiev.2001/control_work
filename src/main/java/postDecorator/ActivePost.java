package postDecorator;

import lombok.Data;

@Data
public class ActivePost extends PostDecorator {
    private ActivePost active;

    public ActivePost(ActivePost active, AbstractPost post) {
        super(post);
        this.active = active;
    }
}
